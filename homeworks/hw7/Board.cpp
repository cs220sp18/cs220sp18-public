//Board.cpp

#include <string>
#include <utility>
#include <map>
#include "Board.h"


using std::string;
using std::map;
using std::pair;
using std::make_pair;



//Returns a string representation of the current board state
string Board::to_string() const {
  const string col_names("abcdefgh");
  const string row_names("12345678");
  string result;

  //append column headers across the top
  result.push_back(' ');
  result.push_back(' ');
  result.push_back(' ');
  for(string::const_iterator it = col_names.begin();
      it != col_names.end(); ++it) {
    result.push_back(' ');
    result.push_back(*it);
    result.push_back(' ');
  }
  result.push_back('\n');

  //append header line below column header line
  result.push_back(' ');
  result.push_back(' ');
  result.push_back(' ');
  for(string::const_iterator it = col_names.begin();
      it != col_names.end(); ++it) {
    result.push_back('-');
    result.push_back('-');
    result.push_back('-');
  }
  result.push_back('\n');
  
  //loop over each row from 8 down to 1
  for(string::const_reverse_iterator row_it = row_names.crbegin();
	row_it != row_names.crend(); ++row_it) {

    //output row header on left side of this row
    result.push_back(*row_it);
    result.push_back(' ');
    result.push_back('|');
    
    //loop over each location in this row
    for(string::const_iterator col_it = col_names.cbegin();
	col_it != col_names.cend(); ++col_it) {

      //deference iterators to create the location name
      string coord;
      coord.push_back(*col_it);
      coord.push_back(*row_it);

      //determine if the current location appears in map
      map<string, char>::const_iterator location = occ.find(coord);

      //output the piece type, if one exists, or a blank
      result.push_back(' ');
      if (location != occ.end()) {
	result.push_back(location->second);
      } else {
	result.push_back(' ');
      }	
      result.push_back(' ');
    }

    //end the current row
    result.push_back('\n');

  }
  return result;
}
