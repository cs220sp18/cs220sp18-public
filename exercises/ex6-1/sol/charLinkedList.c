#include <stdio.h>
#include <stdlib.h>
#include "charLinkedList.h"

Node* create( char value )
{
  Node * node = (Node*)malloc( sizeof( Node ) );
  if( node )
  {
    node->next = NULL;
    node->value = value;
  }
  return node;
}

int insert_after( Node * node , char value )
{
  Node * newNode = create( value );
  if( !newNode ) return 0;
  newNode->next = node->next;
  node->next = newNode;
  return 1;
}

int insert_at_head( Node ** head , char value )
{
  Node * newHead = create( value );
  if( !newHead ) return 0;
  newHead->next = *head;
  *head = newHead;
  return 1;
}

int insert_in_order( Node ** head , char value )
{
  if( !(*head) || value<(*head)->value )
  {
    return insert_at_head( head , value );
  }
  else
  {
    Node * n = *head;
    for( ; n->next!=NULL && n->next->value<value ; n=n->next ){}
    return insert_after( n , value );
  }
  return 0;
}

void remove_after( Node * node )
{
  if( node->next )
  {
    Node * next = node->next;
    node->next = node->next->next;
    free( next );
  }
}

void remove_at_head( Node ** head )
{
  if( *head )
  {
    Node * oldHead = *head;
    *head = (*head)->next;
    free( oldHead );
  }
}

void remove_all( Node ** head , char value )
{
  while( *head && (*head)->value==value )
  {
    remove_at_head( head );
  }
  for( Node * n=*head ; n ; n=n->next )
  {
    while( n->next && n->next->value==value )
    {
      remove_after( n );
    }
  }
}

void print_list( const Node * head )
{
  for( const Node * n=head ; n ; n=n->next ) printf( "%c" , n->value );
}

void free_list( Node * head )
{
  while( head ) remove_at_head( &head );
}
