
#include <iostream>
#include <string>
#include "MyNode.h"
#include "MyList.h"

using std::cout;
using std::endl;

int main() {

  MyList list;

  list.insertAtHead(3);
  list.insertAtHead(2);
  list.insertAtHead(1);

  cout << list << endl;
}

