#include <stdio.h>
#include <stdlib.h>
#include "charLinkedList.h"

Node* create( char value ) {
  Node * node = (Node*)malloc( sizeof( Node ) );
  if( node ) {  //if node is not null, initialize its fields
    node->next = NULL;
    node->value = value;
  }
  return node;  //returns NULL if malloc failed
}

int insert_after( Node * node , char value ) {
  Node * newNode = create( value );
  if( !newNode )
    return 0;
  newNode->next = node->next;
  node->next = newNode;
  return 1;
}

int insert_at_head( Node ** list_ptr , char value ) {
  Node * newHead = create( value );
  if( !newHead ) 
    return 0;
  newHead->next = *list_ptr;
  *list_ptr = newHead;
  return 1;
}

int insert_in_order( Node ** list_ptr , char value ) {
  if( !(*list_ptr) ) {  //head is NULL, so no nodes already in list
    *list_ptr = create( value );
    if( !(*list_ptr) ) 
      return 0;
    else 
      return 1;

  } else if( value < (*list_ptr)->value ) {  //insert before first node
    return insert_at_head( list_ptr , value );

  } else { //insert somewhere after first node; need to figure out where

    //advance pointer n along list until either we're at final node,
    //or until we determine that we should insert immediately after n
    Node * n = *list_ptr;
    while (n->next != NULL && n->next->value < value) {
      n = n->next;
    }
    //notice that we stopped n early enough in loop above that now
    //we can call insert_after
    return insert_after( n , value );
  }
  return 0;
}

void remove_after( Node * node ) {
  if( node->next ) { //don't try to remove a node if one is not there!
    Node * n = node->next;  //n points to node we need to remove
    node->next = node->next->next; //adjust pointer to bypass one we'll remove
    free( n );  //deallocate the node we no longer want
  }
}

void remove_at_head( Node ** list_ptr ) {
  if( *list_ptr ) { //don't try to remove a node if one is not there!
    Node * oldHead = *list_ptr;  //save address of node to remove
    *list_ptr = (*list_ptr)->next; //adjust the head pointer to point to next one
    free( oldHead ); //deallocate the node we no longer want
  }
}

void remove_all( Node ** list_ptr , char value ) {
  //As long as first node in list matches value, remove it
  //Note that there could be several nodes at front which match it,
  //so this is a loop, not an if statement
  //Note use of short-circuit &&: don't look at (*list_ptr)-> value
  //until after you're sure that (*list_ptr) is not NULL!
  while( *list_ptr && (*list_ptr)->value == value ) {
    remove_at_head( list_ptr );
  }

  //Once we reach this point, we know that either list is completely empty,
  //or its first node is not equal to target value.
  //Now, walk a cur pointer through list and remove any match you find
  //at a node just after the cur node.
  for( Node * cur = *list_ptr ; cur ; cur = cur->next ) {

    //short circuit && in use below! Make sure cur->next is not NULL first
    while( cur->next && cur->next->value == value ) {
      remove_after( cur );
    }

  }
}

void print_list( const Node * head ) {
  //loop condition says "keep looping until n is not NULL"
  for( const Node * n=head ; n ; n=n->next ) {
    printf( "%c" , n->value );
  }
}

void free_list( Node * head ) {
  //as long as head points to an actual node, remove it!
  //and remove of course deallocates each node it removes
  while( head ) { 
    remove_at_head( &head );  //note that we send in address of head
  }
}
