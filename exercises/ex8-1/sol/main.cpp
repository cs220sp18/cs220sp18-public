/*
This program illustrates the utility of nested container classes.  

It first collects a series of words from an standard input (or from a
file using Unix input redirection) and counts the frequency of each
word.  The initial data structure used to count the frequency is of
type map<string,int>.

However, it then rearranges the data in the first map into a second
map of type map<int,vector<string>>, so that all words appearing with
the same frequency are grouped together.  A real-world analogy is
making a notebook where each page stores a particular frequency
(i.e. the key value is an int frequency), and also an entire list of
words that appeared with that frequency (i.e. the mapped value is a
vector of strings).  So there is a page representing frequency 0 which
holds all frequency-0 words, and a page representing frequency 1 which
holds all frequency-1 words, and so on.  Each notebook page is a pair
of type <int, vector<string>.  In other words, each notebook page
represents an element in the map.

Note especially the use of iterators throughout this program,
especially how one accesses the items in the nested container using an
iterator.

*/

#include <iostream>
#include <string>
#include <map>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;


int main() {
  string s;
  map<string, int> counters; // store each word and an associated counter

  // read the input, keeping track of each word and how often we see it
  while (cin >> s) {
    //perform a lookup then change mapped value
    ++counters[s];  // same as counters[s] = counters[s] + 1
  }
  

  // loop through the map and print whats in it
  // use it->first and it->second to get current domain and range values
  cout << endl;
  for (map<string, int>::const_iterator it = counters.cbegin();
       it != counters.cend();
       ++it)
    cout << "word " << it->first << " has " << it->second
	 << " occurrences" << endl;



  // make a map of integers to vector of words with that frequency
  map<int, vector<string> > words_by_freq;

  for (map<string, int>::const_iterator it = counters.cbegin();
       it != counters.cend();
       ++it)
    words_by_freq[it->second].push_back(it->first);

  // print out the above map of vectors
  for (map<int, vector<string> >::const_iterator i = words_by_freq.cbegin();
       i != words_by_freq.cend();
       ++i) {
    // use i->first instead of i.first since iterator i is a pointer 
    cout << "\nFrequency: " << i->first << endl;

    // nested for loop to print out all words associated with current frequency
    for (vector<string>::const_iterator j = i->second.cbegin();
	 j != i->second.cend();
	 ++j)
      // use *j below since iterator j is a pointer to string
      cout << *j << endl;
  } 

  return 0;
}
