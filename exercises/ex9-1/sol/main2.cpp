#include "grade_list.h"
#include <iostream>

/**
 * Hint: resolve the TODOs in grade_list.h first.
 * 
 * TODO: This program is *trying* to insert several
 * grades into a GradeList then print the lowest.
 * But it doesn't compile.  Think about why it
 * doesn't work and fix it.  Don't change any
 * private fields to be public.
 */

using std::cout;
using std::endl;
using std::vector;

int main(void) {
  GradeList gl;
  gl.add(20.0); gl.add(30.0); gl.add(40.0);
  gl.add(15.0); gl.add(15.0); gl.add(75.0);
  gl.add(85.0); gl.add(40.0); gl.add(42.0);
  double min_so_far = 100.0;
  for(vector<double>::const_iterator it = gl.cbegin();
      it != gl.cend();
      ++it)
  {
    if(*it < min_so_far) {
      min_so_far = *it;
    }
  }
  cout << "Minimum grade is: " << min_so_far << endl;
  return 0;
}
