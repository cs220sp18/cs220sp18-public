#ifndef CHAR_LINKED_LIST_H
#define CHAR_LINKED_LIST_H

typedef struct _Node {
  char value;
  struct _Node *next;
} Node;

// Returns a pointer to a new node, with "next" member set to NULL and "value" member set to the "value" argument..
// If memory allocation fails, it returns a NULL pointer.
Node* create( char value );

// Inserts a new node with the prescribed value after the node passed in as an argument.
// Return 1 if it succeeds and 0 if it fails.
int insert_after( Node * node , char value );

// Creates a new node with the prescribed value at the beginning of the list and updates the head pointer to point to the new node.
// Returns 1 if it succeeds and 0 if it fails.
int insert_at_head( Node ** list_ptr , char value );

// Creates a new node with the prescribed value and inserts it into the list so that the list remains sorted from smallest to largest.
// Returns 1 if it succeeds and 0 if it fails.
int insert_in_order( Node ** list_ptr , char value );

// Removes the node after the node passed in as an argument from the list and deallocats the memory.
void remove_after( Node * node );

// Removes the node at the head of the list from the list, updates the head pointer to point to the next element in the list, and deallocates the memory.
void remove_at_head( Node ** list_ptr );

// Removes all the nodes whose value matches the argument.
void remove_all( Node ** list_ptr , char value );

// Prints out the contents of the list, from first to last (without whitespaces).
void print_list( const Node * head );

// Frees the contents of the dynamically linked list.
void free_list( Node * head );

#endif // CHAR_LINKED_LIST_H
