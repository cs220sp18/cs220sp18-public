#include <stdio.h>
#include <string.h>

// stdio.h gives us printf
// string.h gives us strlen

int main() {
    // "dna" holds beginning of human mitochrondrial genome sequence
    const char dna[] = "GATCACAGGTCTATCACCCTATTAACCACTCACGGGAGCTCTCCATGCAT"
                       "TTGGTATTTTCGTCTGGGGGGTGTGCACGCGATAGCATTGCGAGACGCTG"
                       "GAGCCGGAGCACCCTATGTCGCAGTATCTGTCTTTGATTCCTGCCTCATT"
                       "CTATTATTTATCGCACCTACGTTCAATATTACAGGCGAACATACCTACTA"
                       "AAGTGTGTTAATTAATTAATGCTTGTAGGACATAATAATAACAATTGAAT";

    // Note: above is one long string literal, wrapped across lines

    int num_a = 0;
    int num_c = 0;
    int num_g = 0;
    int num_t = 0;

    // strlen is a function that returns the length of the string
    int dna_len = strlen(dna);

    for(int i = 0; i < dna_len; i++) {
      char dna_char = dna[i];
      // TODO: count the # of As, Cs, Gs, and Ts
      //       & store results in num_a, num_c, num_g, num_t 
    }

    printf("A:%d, C:%d, G:%d, T:%d\n", num_a, num_c, num_g, num_t);
    return 0;
}
