#include <iostream>
#include "account.h"

//
// Chapter 1: Overloading <<
//

// We often want to print the contents of a class variable
// to an output stream.  E.g. we might want to print "K"
// for a white king in chess, or "Balance: $998, fees: $2"
// for a CheckingAccount.

// In Java, you typically implemented a toString() method
// to accomplish this.  Today we will learn one popular way
// to do this in C++ using the << operator.

// TODO: This will not compile.  Although a virtual `type`
//       member function is already implemented, you still
//       need to implement operator<<.
//
//       Implement it so that it prints:
//          "<type>: <balance>"
//       where <type> is the account type, as returned by
//       the type() member function, and <balance> is the
//       current balance, as returned by get_balance().
//
//       Recall that implementing operator<< is 
//       complicated by the fact that the return value and
//       the left-hand operand both have
//       type std::ostream&, whereas the right-hand
//       operand has the type of the object to be printed
//
//       Think about the following questions while working:
//
//       - Why does operator<<() have to be implemented as
//         a function outside of the class?
//       - What role does the "friend" keyword play?  Is it
//         needed here?
//       - Does operator<<() have to be implemented for the
//         three classes separately, or can it be
//         implemented just in Account?
//       - What role do virtual functions play here?  Are
//         they doing anything to make your life easier?

using std::cout;
using std::endl;

void print_types_1(const Account& acct,
                   const CheckingAccount& checking,
                   const SavingsAccount& saving)
{
  cout << "print_types_1:" << endl
       << "Account: "  << acct << endl
       << "Checking: " << checking << endl
       << "Saving: "   << saving   << endl;
}

void print_types_2(const Account& acct,
                   const Account& checking,
                   const Account& saving)
{
  cout << "print_types_2:" << endl
       << "Account: "  << acct << endl
       << "Checking: " << checking << endl
       << "Saving: "   << saving   << endl;
}

int main() {
  Account acct(1000.0);
  CheckingAccount checking(1000.0, 2.00);
  SavingsAccount saving(1000.0, 0.05);

  print_types_1(acct, checking, saving);
  print_types_2(acct, checking, saving);
  
  return 0;
}
