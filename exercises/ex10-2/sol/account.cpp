#include "account.h"

using std::ostream;
using std::cout;
using std::endl;

ostream& operator<<(ostream& os, const Account& c) {
    os << c.type() << ": " << c.get_balance();
    return os;
}
