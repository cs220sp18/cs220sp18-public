#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include "card.h"

//
// Part 1: Read and write individual cards
//
// TODO: Do all the TODOs in card.h and card.cpp and look over the
//       three test cases below.  Get this file to compile, run, and
//       print "All assertions passed."  DO NOT CHANGE THIS FILE.
//

using std::cout;
using std::endl;
using std::string;
using std::stringstream;

int main() {
    // A stringstream is like a stream, but instead of reading and
    // writing from the console/user, you are reading and writing a
    // string ("buffer") held inside the stringstream object

    // After this exercise, it should be obvious how stringstream can
    // be useful for testing

    {
        // Test case 1:
        Card c(Rank::KING, Suit::SPADE);
        stringstream output;
        output << c;
        assert(output.str() == "King of Spades");
    }

    {
        // Test case 2:
        stringstream input("Ace of Clubs");
        Card c;
        input >> c;
        assert(c.rank == Rank::ACE);
        assert(c.suit == Suit::CLUB);
    }

    {
        // Test case 3:
        stringstream input("Eight of Hearts"), output;
        Card c;
        input >> c;
        output << c;
        assert(output.str() == "Eight of Hearts");
    }

    cout << "All assertions passed" << endl;

    return 0;
}
