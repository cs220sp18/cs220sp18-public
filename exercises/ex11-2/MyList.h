#ifndef MYLIST_H
#define MYLIST_H

#include <iostream>
#include "MyNode.h"

//A linked list hold int data
class MyList {

private:
  MyNode* head;  //pointer to first node in linked list

public:
  MyList():head(nullptr) {}  //create empty linked list

  void insertAtHead(int d); //create new MyNode and add it at head
  
  void insertAtTail(int d); //create new MyNode and add it at head

  //output the entire list, with data elements separated by spaces
  friend std::ostream& operator<<(std::ostream& os, const MyList& list);

};

#endif
