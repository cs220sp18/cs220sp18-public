#ifndef STACK_H
#define STACK_H


// [TODO 1]
// Declare a templated stack class, called "stack" here.
// The template parameter should indicate what type of data is stored within the class.
// The class should have a default constructor and destructor as well as push, pop, and empty member functions:
//    push -- adds an element to the back of the stack
//    pop -- removes and returns the back element of the stack
//    empty -- indicates whether or not the stack is empty


// Put your class declaration here

// Put the member function definitions in "stack.inc"
#include "stack.inc"
#endif // STACK_H
