#include <stdio.h>
#include <stdlib.h>
#include "charLinkedList.h"

Node* create( char value )
{
  Node * node = (Node*)malloc( sizeof( Node ) );
  if( node )
  {
    node->next = NULL;
    node->value = value;
  }
  return node;
}

int insert_after( Node * node , char value )
{
  Node * newNode = create( value );
  if( !newNode ) return 0;
  newNode->next = node->next;
  node->next = newNode;
  return 1;
}

int insert_at_head( Node ** list_ptr , char value )
{
  Node * newHead = create( value );
  if( !newHead ) return 0;
  newHead->next = *list_ptr;
  *list_ptr = newHead;
  return 1;
}

int insert_in_order( Node ** list_ptr , char value )
{
  // [WARNING] This is not implemented.
  // Until fixed, this defaults to insert_at_head
  return insert_at_head( list_ptr , value );
}

void remove_after( Node * node )
{
  // [WARNING] This is not implemented.
}

void remove_at_head( Node ** list_ptr )
{
  // [WARNING] This is not implemented.
}

void remove_all( Node ** list_ptr , char value )
{
  // [WARNING] This is not implemented.
}

void print_list( const Node * head )
{
  for( const Node * n=head ; n ; n=n->next ) printf( "%c" , n->value );
}

void free_list( Node * head )
{
  while( head )
  {
    Node * newHead = head->next;
    free( head );
    head = newHead;
  }
}
