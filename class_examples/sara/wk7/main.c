#include <stdio.h>
#include <stdlib.h>
#include "int_list.h"

void output_list(Node * head);
void output_list_recursively(Node * head);

int main() {

  Node* head = NULL;
  insert_head(&head, 3);
  output_list(head);
  //  output_list_recursively(head);

  insert_after(head, 10);
  output_list(head);

  insert_after(head, 8);
  output_list(head);

  insert_head(&head, 1);
  output_list(head);

  free_list(head);
  head = NULL;  //free doesn't do this for us
  
}

void output_list(Node * head){
  Node * cur = head; //make cur point to same node that head points to
  while (cur != NULL) {
    printf("%d ", cur->data); //output int at the node cur points to
    cur = cur->next;  //advance cur to point to the next node in the list
  }
  printf("\n");
}


void output_list_recursively(Node * head){
  //base case
  if (head == NULL){  //list is empty
    printf("\n");     //just end the line
    return;
  }
  //recursive case: print one item, then make recursive call on smaller list
  printf("%d ", head->data);
  output_list_recursively(head->next);

}

