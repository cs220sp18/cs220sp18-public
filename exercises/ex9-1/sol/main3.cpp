/**
 * Hint: resolve the TODOs in grade_list.h first.
 * 
 * TODO: Write a program that declares a GradeList
 *       variable and adds to it all the even
 *       numbers 0-100:
 *       
 *       {0, 2, 4, ..., 98, 100}
 * 
 *       then prints out the minimum, maximum,
 *       median and 75th percentile.
 */

#include <iostream>
#include "grade_list.h"

using std::cout;
using std::endl;

int main() {
  GradeList gl;
  for(int i = 0; i <= 100; i += 2) {
    gl.add((double)i);
  }
  cout << "Min:  " << gl.percentile(0.0)   << endl
       << "Max:  " << gl.percentile(100.0) << endl
       << "Med:  " << gl.percentile(50.0)  << endl
       << "75th: " << gl.percentile(75.0)  << endl;
  return 0;
}
