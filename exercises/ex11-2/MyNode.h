#ifndef MYNODE_H
#define MYNODE_H

//A single node that holds int data; appropriate
//for creating a linked list 
class MyNode {

  public:
  
    int data;     //the payload

    MyNode *next; //pointer to following node

    
    //two-argument constructor
    MyNode(int d, MyNode* n) : data(d), next(n) {}


};  

#endif
