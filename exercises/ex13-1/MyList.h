#ifndef MYLIST_H
#define MYLIST_H

#include <iostream>
#include "MyNode.h"

// A linked list template

template<typename T>
class MyList {

private:
    MyNode<T>* head; // pointer to first node in linked list

public:

    // *** DON'T CHANGE THIS FILE ABOVE THIS POINT ***

    // For the nested iterator class `iterator`, you will have to
    // provide implementations for the member functions commented out.
    //
    // For the nested iterator class `const_iterator`, you will have
    // to implement something like `iterator` but with additional
    // `const` constraints to disallow modification of the elements
    // via operator*().
    //
    // You will have to un-comment and implement the begin(),
    // end(), cbegin() and cend() member functions below
    //
    // You will also need to un-comment and implement the MyList
    // constructor taking a begin and end iterator.

    class iterator {
        MyNode<T>* it;

      public:
        iterator(MyNode<T>* initial) : it(initial) { }

        // iterator& operator++() { ... }

        // bool operator!=(const iterator& o) const { ... }

        // T& operator*() { ... }
    };

    // class const_iterator {
    //     ...
    // };

    // iterator begin() { ... }
    // iterator end() { ... }
    
    // const_iterator cbegin() const { ... }
    // const_iterator cend() const { ... }

    // Constructor for MyList that sets the linked list
    // to contain the elements in the container with begin
    // and end iterators provided as arguments.
    template< typename Iterator >
    MyList< T >( Iterator iBegin , Iterator iEnd );

    // *** DON'T CHANGE THIS FILE BELOW THIS POINT ***

    MyList<T>() : head(nullptr) {}  // create empty linked list
    
    ~MyList<T>();                   // deallocate all nodes

    void insertAtHead(const T& d);  // create new MyNode and add at head

    void insertAtTail(const T& d);  // create new MyNode and add at tail

    // get const pointer to head node
    const MyNode<T>* get_head() const { return head; }
};

#include "MyList.inc"
#endif
