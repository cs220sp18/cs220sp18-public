#include <stdio.h>
#include <stdlib.h>
#include "readLine.h"

/*
 * Overview:
 *   This code will read the lines from a file, one by one and then output the lines
 *   to the standard output, writing out the line number before each line.
 * Command line arguments:
 *   If a command line argument is provided, the executable assumes that is the name
 *   of the file and will read from there.
 *   Otherwise, the executable will read the lines from the standard input.
 */
int main( int argc , char* argv[] )
{
  FILE* fp = NULL;  // The file pointer from which we read
  // If no command line arguments are specified, fail.
  if( argc==1 )
    {
      fprintf( stderr , "[ERROR] Expected file name\n" );
      return 1;
    }
  // Otherwise, the first argument is the name of the file from which to read.
  else
    {
      fp = fopen( argv[1] , "r" );
      if( !fp )
	{
	  fprintf( stderr , "[ERROR] Could not open file for reading: %s\n" , argv[1] );
	  return 1;
	}
    }
  char* line = NULL;
  // Read in the next line from the file (until there is nothing to read).
  while( (line=readLine( fp ) ) )
    {
      // Write the line to the standard output
      printf( "%s" , line );
      // We must deallocate the string since it was allocated dynamically
      // in the readLine function.
      free( line );
    }
  // If we opened the file-handle, we need to close it.
  if( argc>1 ){ fclose( fp ); }
  return 0;
}
