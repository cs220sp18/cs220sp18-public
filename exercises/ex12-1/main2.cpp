#include <iostream>
#include <string>
#include "stack.h"
#include "card.h"

// [TODO 3]
// Define a function call "pop_all_and_print" here.
// It should take a single argument -- a reference to a stack, and should
// successively pop off the elements of the stack and print them to stdout


int main( void )
{
  stack< int > iStack;
  stack< Card > cStack;
  for( int i=0 ; i<5 ; i++ )
  {
    iStack.push(i);
    cStack.push( Card( Rank((int)Rank::ACE+i) , Suit::HEART ) );
  }

  std::cout << "Printing integers:" << std::endl;
  pop_all_and_print( iStack );

  std::cout << "Printing cards:" << std::endl;
  // [TODO 4]
  // Uncomment the next line
  // pop_all_and_print( cStack );
  return 0;
}

