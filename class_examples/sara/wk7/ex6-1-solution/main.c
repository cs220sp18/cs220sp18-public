#include <stdio.h>
#include <stdlib.h>
#include "charLinkedList.h"

int main( void ) {
  Node * head = NULL;

  // Read in the characters and construct a linked list
  printf( "Enter a string of characters: " );
  while( 1 ) {
    char c = getc( stdin );
    if( c!=EOF && c!='\n' ) {
      if( !insert_in_order( &head , c ) ) {
      	fprintf( stderr , "[ERROR] Failed to insert_in_order: %c\n" , c );
	      return 1;
      }
    } else {
      break;
    }
  }

  printf( "Contents [sorted]: {" );
  print_list( head );
  printf( "}\n" );

  // Remove the first and last elements
  remove_at_head( &head );
  if( head->next ) {
    Node * n = head;
    while( n->next->next ){ 
      n = n->next; 
    }
    remove_after( n );
  }
  printf( "Contents [first and last removed]: {" );
  print_list( head );
  printf( "}\n" );

  // Remove all the vowels
  remove_all( &head , 'a' );
  remove_all( &head , 'e' );
  remove_all( &head , 'i' );
  remove_all( &head , 'o' );
  remove_all( &head , 'u' );
  remove_all( &head , 'A' );
  remove_all( &head , 'E' );
  remove_all( &head , 'I' );
  remove_all( &head , 'O' );
  remove_all( &head , 'U' );

  printf( "Contents [no vowels]: {" );
  print_list( head );
  printf( "}\n" );
  
  free_list( head );
  
  return 0;
}
