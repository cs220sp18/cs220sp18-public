#ifndef CARD_H
#define CARD_H

// REMEMBER: NO `using` STATEMENTS in .h files, please

// `enum class` declares a new categorical type, with the
// categories listed in curly braces.  By convention, we
// use all caps for categories (e.g. DIAMOND).

enum class Suit { HEART, CLUB, DIAMOND, SPADE };

// Refer to Suit categories by: Suit::HEART, Suit::CLUB,
// Suit::DIAMOND, Suit::SPADE

enum class Rank {
    // "= 1" to start numbering at 1 instead of 0
    ACE = 1, TWO, THREE, FOUR, FIVE, SIX, SEVEN,
    EIGHT, NINE, TEN, JACK, QUEEN, KING
};

// Refer to Rank categories by: Rank::ACE, Rank::TWO, ...

// In C++, a struct acts just like a class but with `public:`
// as the default access modifier.  You'll often see `struct`s
// used for very simple types like this.
struct Card {
    Rank rank; // public
    Suit suit; // public

    Card() : Card(Rank::ACE, Suit::HEART)   { } // public
    Card(Rank r, Suit s) : rank(r), suit(s) { } // public 
    
    // *** solution code ***
    bool operator<(Card o) const {
        if(suit < o.suit) {
            return true;
        } else if(suit > o.suit) {
            return false;
        }
        return rank < o.rank;
    }
    // *** solution code ***
};

// TODO: Suit: add prototype for operator<< (insertion)

// *** solution code ***
std::ostream& operator<<(std::ostream& os, Suit s);
// *** solution code ***

// TODO: Suit: add prototype for operator>> (extraction)

// *** solution code ***
std::istream& operator>>(std::istream& is, Suit& s);
// *** solution code ***

// TODO: Rank: add prototype for operator<< (insertion)

// *** solution code ***
std::ostream& operator<<(std::ostream& os, Rank r);
// *** solution code ***

// TODO: Rank: add prototype for operator>> (extraction)

// *** solution code ***
std::istream& operator>>(std::istream& is, Rank& r);
// *** solution code ***

// TODO: Card: add prototype for operator<< (insertion)

// *** solution code ***
std::ostream& operator<<(std::ostream& os, Card c);
// *** solution code ***

// TODO: Card: add prototype for operator>> (extraction)

// *** solution code ***
std::istream& operator>>(std::istream& is, Card& c);
// *** solution code ***

#endif
