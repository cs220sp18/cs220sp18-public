#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readLine.h"

/*
 * This function reads the next line from the file handle fp.
 * (That is, it reads either up to the next newline character, or to the end of the file, whichever comes first.)
 * If fp is at the end of the file when the function is called, the function returns NULL.
 * Otherwise, it returns the null-terminated string that was read (including the newline character)
 */

char* readLine( FILE* fp )
{

  // The function is implemented by making successive calls to the fgets function
  // and dynamically growing a character arrray that contains the line.
  

  char buffer[10];   // The buffer into which fgets reads. (Do _not_ change the size of this array.)
  char* line = NULL; // The character array / string that stores the line

  // Repeatedly call fgets until you have read the entire line (or until you reach the end-of-file).
  while( fgets( buffer , 10 , fp ) )
    {
      /////////////////////////
      // YOUR CODE GOES HERE //
      /////////////////////////
    }

  return line;
}
