#include <stdlib.h>
#include "int_list.h"

//Creates and inserts a new node containing value as its data,
//placing that new node just after the node pointed to by n.
//Returns 0 if successful, nonzero otherwise
int insert_after(Node * n, int value) {
  Node * new_node = malloc(sizeof(Node));
  if (!new_node) {  //if new_node is NULL, then malloc failed
    return 1;
  }

  new_node->data = value;
  new_node->next = n->next; //make new one point to what n->next was pointing to
  n->next = new_node; //make n->next point to new item

  return 0;
}

