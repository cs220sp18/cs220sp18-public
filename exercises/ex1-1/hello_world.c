// hello_world.c

#include <stdio.h>

int main() {

   // Print "Hello, world!" followed by newline and then exit
   printf("Hello, world!\n");
   return 0;

}
